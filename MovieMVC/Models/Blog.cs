namespace MovieMVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Blog")]
    public partial class Blog
    {
        [StringLength(10)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }

        [Required, StringLength(60, MinimumLength = 3)]
        public string Title { get; set; }

        [Required, StringLength(60, MinimumLength = 3)]
        public string Describe { get; set; }

   
        public string Detail { get; set; }

        public string Image { get; set; }

        public string Location { get; set; }
        public string[] LocationList { get; set; }

        public bool? Status { get; set; }

        public string Type { get; set; }

        [Column(TypeName = "date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DatePublic { get; set; }
    }
}
